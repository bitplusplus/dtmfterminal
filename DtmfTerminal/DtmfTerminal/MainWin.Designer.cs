﻿namespace DtmfTerminal
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            this.panelmainwin = new System.Windows.Forms.Panel();
            this.buttre = new System.Windows.Forms.Button();
            this.buttzv = new System.Windows.Forms.Button();
            this.buttD = new System.Windows.Forms.Button();
            this.buttC = new System.Windows.Forms.Button();
            this.buttB = new System.Windows.Forms.Button();
            this.buttA = new System.Windows.Forms.Button();
            this.butt0 = new System.Windows.Forms.Button();
            this.butt9 = new System.Windows.Forms.Button();
            this.butt8 = new System.Windows.Forms.Button();
            this.butt7 = new System.Windows.Forms.Button();
            this.butt6 = new System.Windows.Forms.Button();
            this.butt5 = new System.Windows.Forms.Button();
            this.butt4 = new System.Windows.Forms.Button();
            this.butt3 = new System.Windows.Forms.Button();
            this.butt2 = new System.Windows.Forms.Button();
            this.butt1 = new System.Windows.Forms.Button();
            this.textBoxsend = new System.Windows.Forms.TextBox();
            this.splittermainwin = new System.Windows.Forms.Splitter();
            this.richTextBoxinout = new System.Windows.Forms.RichTextBox();
            this.buttonsend = new System.Windows.Forms.Button();
            this.statusStripmainwin = new System.Windows.Forms.StatusStrip();
            this.toolStripmainwin = new System.Windows.Forms.ToolStrip();
            this.chartwaveinout = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelmainwin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartwaveinout)).BeginInit();
            this.SuspendLayout();
            // 
            // panelmainwin
            // 
            this.panelmainwin.Controls.Add(this.chartwaveinout);
            this.panelmainwin.Controls.Add(this.buttre);
            this.panelmainwin.Controls.Add(this.buttzv);
            this.panelmainwin.Controls.Add(this.buttD);
            this.panelmainwin.Controls.Add(this.buttC);
            this.panelmainwin.Controls.Add(this.buttB);
            this.panelmainwin.Controls.Add(this.buttA);
            this.panelmainwin.Controls.Add(this.butt0);
            this.panelmainwin.Controls.Add(this.butt9);
            this.panelmainwin.Controls.Add(this.butt8);
            this.panelmainwin.Controls.Add(this.butt7);
            this.panelmainwin.Controls.Add(this.butt6);
            this.panelmainwin.Controls.Add(this.butt5);
            this.panelmainwin.Controls.Add(this.butt4);
            this.panelmainwin.Controls.Add(this.butt3);
            this.panelmainwin.Controls.Add(this.butt2);
            this.panelmainwin.Controls.Add(this.butt1);
            this.panelmainwin.Controls.Add(this.textBoxsend);
            this.panelmainwin.Controls.Add(this.splittermainwin);
            this.panelmainwin.Controls.Add(this.richTextBoxinout);
            this.panelmainwin.Controls.Add(this.buttonsend);
            this.panelmainwin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelmainwin.Location = new System.Drawing.Point(0, 25);
            this.panelmainwin.Name = "panelmainwin";
            this.panelmainwin.Size = new System.Drawing.Size(784, 514);
            this.panelmainwin.TabIndex = 0;
            // 
            // buttre
            // 
            this.buttre.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttre.ForeColor = System.Drawing.Color.Blue;
            this.buttre.Location = new System.Drawing.Point(636, 403);
            this.buttre.Name = "buttre";
            this.buttre.Size = new System.Drawing.Size(36, 33);
            this.buttre.TabIndex = 19;
            this.buttre.Text = "#";
            this.buttre.UseVisualStyleBackColor = true;
            this.buttre.Click += new System.EventHandler(this.buttre_Click);
            // 
            // buttzv
            // 
            this.buttzv.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttzv.ForeColor = System.Drawing.Color.Blue;
            this.buttzv.Location = new System.Drawing.Point(594, 403);
            this.buttzv.Name = "buttzv";
            this.buttzv.Size = new System.Drawing.Size(36, 33);
            this.buttzv.TabIndex = 18;
            this.buttzv.Text = "*";
            this.buttzv.UseVisualStyleBackColor = true;
            this.buttzv.Click += new System.EventHandler(this.buttzv_Click);
            // 
            // buttD
            // 
            this.buttD.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttD.ForeColor = System.Drawing.Color.Blue;
            this.buttD.Location = new System.Drawing.Point(552, 403);
            this.buttD.Name = "buttD";
            this.buttD.Size = new System.Drawing.Size(36, 33);
            this.buttD.TabIndex = 17;
            this.buttD.Text = "D";
            this.buttD.UseVisualStyleBackColor = true;
            this.buttD.Click += new System.EventHandler(this.buttD_Click);
            // 
            // buttC
            // 
            this.buttC.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttC.ForeColor = System.Drawing.Color.Blue;
            this.buttC.Location = new System.Drawing.Point(510, 403);
            this.buttC.Name = "buttC";
            this.buttC.Size = new System.Drawing.Size(36, 33);
            this.buttC.TabIndex = 16;
            this.buttC.Text = "C";
            this.buttC.UseVisualStyleBackColor = true;
            this.buttC.Click += new System.EventHandler(this.buttC_Click);
            // 
            // buttB
            // 
            this.buttB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttB.ForeColor = System.Drawing.Color.Blue;
            this.buttB.Location = new System.Drawing.Point(468, 403);
            this.buttB.Name = "buttB";
            this.buttB.Size = new System.Drawing.Size(36, 33);
            this.buttB.TabIndex = 15;
            this.buttB.Text = "B";
            this.buttB.UseVisualStyleBackColor = true;
            this.buttB.Click += new System.EventHandler(this.buttB_Click);
            // 
            // buttA
            // 
            this.buttA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttA.ForeColor = System.Drawing.Color.Blue;
            this.buttA.Location = new System.Drawing.Point(426, 403);
            this.buttA.Name = "buttA";
            this.buttA.Size = new System.Drawing.Size(36, 33);
            this.buttA.TabIndex = 14;
            this.buttA.Text = "A";
            this.buttA.UseVisualStyleBackColor = true;
            this.buttA.Click += new System.EventHandler(this.buttA_Click);
            // 
            // butt0
            // 
            this.butt0.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt0.ForeColor = System.Drawing.Color.Blue;
            this.butt0.Location = new System.Drawing.Point(384, 403);
            this.butt0.Name = "butt0";
            this.butt0.Size = new System.Drawing.Size(36, 33);
            this.butt0.TabIndex = 13;
            this.butt0.Text = "0";
            this.butt0.UseVisualStyleBackColor = true;
            this.butt0.Click += new System.EventHandler(this.butt0_Click);
            // 
            // butt9
            // 
            this.butt9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt9.ForeColor = System.Drawing.Color.Blue;
            this.butt9.Location = new System.Drawing.Point(342, 403);
            this.butt9.Name = "butt9";
            this.butt9.Size = new System.Drawing.Size(36, 33);
            this.butt9.TabIndex = 12;
            this.butt9.Text = "9";
            this.butt9.UseVisualStyleBackColor = true;
            this.butt9.Click += new System.EventHandler(this.butt9_Click);
            // 
            // butt8
            // 
            this.butt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt8.ForeColor = System.Drawing.Color.Blue;
            this.butt8.Location = new System.Drawing.Point(300, 403);
            this.butt8.Name = "butt8";
            this.butt8.Size = new System.Drawing.Size(36, 33);
            this.butt8.TabIndex = 11;
            this.butt8.Text = "8";
            this.butt8.UseVisualStyleBackColor = true;
            this.butt8.Click += new System.EventHandler(this.butt8_Click);
            // 
            // butt7
            // 
            this.butt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt7.ForeColor = System.Drawing.Color.Blue;
            this.butt7.Location = new System.Drawing.Point(258, 403);
            this.butt7.Name = "butt7";
            this.butt7.Size = new System.Drawing.Size(36, 33);
            this.butt7.TabIndex = 10;
            this.butt7.Text = "7";
            this.butt7.UseVisualStyleBackColor = true;
            this.butt7.Click += new System.EventHandler(this.butt7_Click);
            // 
            // butt6
            // 
            this.butt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt6.ForeColor = System.Drawing.Color.Blue;
            this.butt6.Location = new System.Drawing.Point(216, 403);
            this.butt6.Name = "butt6";
            this.butt6.Size = new System.Drawing.Size(36, 33);
            this.butt6.TabIndex = 9;
            this.butt6.Text = "6";
            this.butt6.UseVisualStyleBackColor = true;
            this.butt6.Click += new System.EventHandler(this.butt6_Click);
            // 
            // butt5
            // 
            this.butt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt5.ForeColor = System.Drawing.Color.Blue;
            this.butt5.Location = new System.Drawing.Point(174, 403);
            this.butt5.Name = "butt5";
            this.butt5.Size = new System.Drawing.Size(36, 33);
            this.butt5.TabIndex = 8;
            this.butt5.Text = "5";
            this.butt5.UseVisualStyleBackColor = true;
            this.butt5.Click += new System.EventHandler(this.butt5_Click);
            // 
            // butt4
            // 
            this.butt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt4.ForeColor = System.Drawing.Color.Blue;
            this.butt4.Location = new System.Drawing.Point(132, 403);
            this.butt4.Name = "butt4";
            this.butt4.Size = new System.Drawing.Size(36, 33);
            this.butt4.TabIndex = 7;
            this.butt4.Text = "4";
            this.butt4.UseVisualStyleBackColor = true;
            this.butt4.Click += new System.EventHandler(this.butt4_Click);
            // 
            // butt3
            // 
            this.butt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt3.ForeColor = System.Drawing.Color.Blue;
            this.butt3.Location = new System.Drawing.Point(90, 403);
            this.butt3.Name = "butt3";
            this.butt3.Size = new System.Drawing.Size(36, 33);
            this.butt3.TabIndex = 6;
            this.butt3.Text = "3";
            this.butt3.UseVisualStyleBackColor = true;
            this.butt3.Click += new System.EventHandler(this.butt3_Click);
            // 
            // butt2
            // 
            this.butt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt2.ForeColor = System.Drawing.Color.Blue;
            this.butt2.Location = new System.Drawing.Point(48, 403);
            this.butt2.Name = "butt2";
            this.butt2.Size = new System.Drawing.Size(36, 33);
            this.butt2.TabIndex = 5;
            this.butt2.Text = "2";
            this.butt2.UseVisualStyleBackColor = true;
            this.butt2.Click += new System.EventHandler(this.butt2_Click);
            // 
            // butt1
            // 
            this.butt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butt1.ForeColor = System.Drawing.Color.Blue;
            this.butt1.Location = new System.Drawing.Point(6, 403);
            this.butt1.Name = "butt1";
            this.butt1.Size = new System.Drawing.Size(36, 33);
            this.butt1.TabIndex = 4;
            this.butt1.Text = "1";
            this.butt1.UseVisualStyleBackColor = true;
            this.butt1.Click += new System.EventHandler(this.butt1_Click);
            // 
            // textBoxsend
            // 
            this.textBoxsend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.textBoxsend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxsend.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxsend.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBoxsend.Location = new System.Drawing.Point(0, 442);
            this.textBoxsend.MaxLength = 255;
            this.textBoxsend.Name = "textBoxsend";
            this.textBoxsend.Size = new System.Drawing.Size(784, 31);
            this.textBoxsend.TabIndex = 2;
            // 
            // splittermainwin
            // 
            this.splittermainwin.Dock = System.Windows.Forms.DockStyle.Top;
            this.splittermainwin.Location = new System.Drawing.Point(0, 134);
            this.splittermainwin.Name = "splittermainwin";
            this.splittermainwin.Size = new System.Drawing.Size(784, 3);
            this.splittermainwin.TabIndex = 1;
            this.splittermainwin.TabStop = false;
            // 
            // richTextBoxinout
            // 
            this.richTextBoxinout.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBoxinout.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxinout.Name = "richTextBoxinout";
            this.richTextBoxinout.Size = new System.Drawing.Size(784, 134);
            this.richTextBoxinout.TabIndex = 0;
            this.richTextBoxinout.Text = "";
            // 
            // buttonsend
            // 
            this.buttonsend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonsend.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonsend.Location = new System.Drawing.Point(0, 473);
            this.buttonsend.Name = "buttonsend";
            this.buttonsend.Size = new System.Drawing.Size(784, 41);
            this.buttonsend.TabIndex = 3;
            this.buttonsend.Text = "Отправить";
            this.buttonsend.UseVisualStyleBackColor = true;
            this.buttonsend.Click += new System.EventHandler(this.buttonsend_Click);
            // 
            // statusStripmainwin
            // 
            this.statusStripmainwin.Location = new System.Drawing.Point(0, 539);
            this.statusStripmainwin.Name = "statusStripmainwin";
            this.statusStripmainwin.Size = new System.Drawing.Size(784, 22);
            this.statusStripmainwin.TabIndex = 1;
            this.statusStripmainwin.Text = "Панель статуса";
            // 
            // toolStripmainwin
            // 
            this.toolStripmainwin.Location = new System.Drawing.Point(0, 0);
            this.toolStripmainwin.Name = "toolStripmainwin";
            this.toolStripmainwin.Size = new System.Drawing.Size(784, 25);
            this.toolStripmainwin.TabIndex = 2;
            this.toolStripmainwin.Text = "Панель управления";
            // 
            // chartwaveinout
            // 
            chartArea2.Name = "ChartArea1";
            this.chartwaveinout.ChartAreas.Add(chartArea2);
            this.chartwaveinout.Dock = System.Windows.Forms.DockStyle.Top;
            this.chartwaveinout.Location = new System.Drawing.Point(0, 137);
            this.chartwaveinout.Name = "chartwaveinout";
            this.chartwaveinout.Size = new System.Drawing.Size(784, 260);
            this.chartwaveinout.TabIndex = 20;
            this.chartwaveinout.Text = "График";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panelmainwin);
            this.Controls.Add(this.toolStripmainwin);
            this.Controls.Add(this.statusStripmainwin);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DTMF Terminal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelmainwin.ResumeLayout(false);
            this.panelmainwin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartwaveinout)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelmainwin;
        private System.Windows.Forms.StatusStrip statusStripmainwin;
        private System.Windows.Forms.ToolStrip toolStripmainwin;
        private System.Windows.Forms.Button buttonsend;
        private System.Windows.Forms.TextBox textBoxsend;
        private System.Windows.Forms.Splitter splittermainwin;
        private System.Windows.Forms.RichTextBox richTextBoxinout;
        private System.Windows.Forms.Button butt0;
        private System.Windows.Forms.Button butt9;
        private System.Windows.Forms.Button butt8;
        private System.Windows.Forms.Button butt7;
        private System.Windows.Forms.Button butt6;
        private System.Windows.Forms.Button butt5;
        private System.Windows.Forms.Button butt4;
        private System.Windows.Forms.Button butt3;
        private System.Windows.Forms.Button butt2;
        private System.Windows.Forms.Button butt1;
        private System.Windows.Forms.Button buttzv;
        private System.Windows.Forms.Button buttD;
        private System.Windows.Forms.Button buttC;
        private System.Windows.Forms.Button buttB;
        private System.Windows.Forms.Button buttA;
        private System.Windows.Forms.Button buttre;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartwaveinout;
    }
}

