﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NAudio.Wave;
using NAudio.FileFormats;
using NAudio.CoreAudioApi;
using NAudio;
using System.Threading;


namespace DtmfTerminal
{
    public partial class MainForm : Form
    {

        // WaveIn - поток для записи
        WaveIn waveIn;

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonsend_Click(object sender, EventArgs e)
        {
            const short FRAME_SIZE = 160;
            short[] shortsArray = new short[FRAME_SIZE];
            MemoryStream mstr = new MemoryStream();
            DtmfGenerator dtmfGenerator = new DtmfGenerator(FRAME_SIZE, 100, 50);

            dtmfGenerator.NUMBER_BUTTONS = textBoxsend.Text.Length;
            char[] dialButtons = new char[dtmfGenerator.NUMBER_BUTTONS];


            String strsr = textBoxsend.Text.ToString();
            for (int i = 0; i < strsr.Length; i++)
            {
                dialButtons[i] = strsr[i];
            }
            if (strsr.Length <= 0) return;

            dtmfGenerator.transmitNewDialButtonsArray(dialButtons, dialButtons.Length);
            while (!dtmfGenerator.getReadyFlag())
            {
                // send dtmf >>
                dtmfGenerator.dtmfGenerating(shortsArray);
                mstr.Seek(mstr.Length, SeekOrigin.Begin);
                for (int ii = 0; ii < shortsArray.Length; ii++)
                {
                    mstr.WriteByte((byte)(shortsArray[ii] & 0xff));
                    mstr.WriteByte((byte)((shortsArray[ii] >> 8) & 0xff));
                }
                // send dtmf <<

            }//while

            dtmfGenerator.PlayDTMF(mstr.ToArray());

           
            mstr.Close();

        }//button send

        private void butt1_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "1";
        }

        private void butt2_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "2";
        }

        private void butt3_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "3";
        }

        private void butt4_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "4";
        }

        private void butt5_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "5";
        }

        private void butt6_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "6";
        }

        private void butt7_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "7";
        }

        private void butt8_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "8";
        }

        private void butt9_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "9";
        }

        private void butt0_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "0";
        }

        private void buttA_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "A";
        }

        private void buttB_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "B";
        }

        private void buttC_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "C";
        }

        private void buttD_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "D";
        }

        private void buttzv_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "*";
        }

        private void buttre_Click(object sender, EventArgs e)
        {
            textBoxsend.Text += "#";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            try
            {

                waveIn = new WaveIn();
                //Дефолтное устройство для записи (если оно имеется)
                waveIn.DeviceNumber = 0;
                //Прикрепляем к событию DataAvailable обработчик, возникающий при наличии записываемых данных
                waveIn.DataAvailable += waveIn_DataAvailable;
                //Прикрепляем обработчик завершения записи
                waveIn.RecordingStopped += waveIn_RecordingStopped;
                //Формат wav-файла - принимает параметры - частоту дискретизации и количество каналов(здесь mono)
                waveIn.WaveFormat = new WaveFormat(8000, 1);

                //Начало записи
                waveIn.StartRecording();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            chartwaveinout.Series.Add("SoundIn");
            chartwaveinout.Series["SoundIn"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chartwaveinout.Series["SoundIn"].ChartArea = "ChartArea1";



        }//load
        //Окончание записи
        void waveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<StoppedEventArgs>(waveIn_RecordingStopped), sender, e);
            }
            else
            {
                waveIn.Dispose();
                waveIn = null;
            }

        }
        //Получение данных из входного буфера 
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<WaveInEventArgs>(waveIn_DataAvailable), sender, e);
            }
            else
            {
                //Записываем данные из буфера в файл
                DtmfDetector dtmfDetector = new DtmfDetector(e.BytesRecorded / 2);
                short[] shortsArray = new short[e.BytesRecorded / 2 + 1];
                chartwaveinout.Series["SoundIn"].Points.Clear();
                for (int cp = 0, cp1 = 0; cp1 < e.BytesRecorded / 2; cp1++)
                {
                    shortsArray[cp1] = (short)((short)(e.Buffer[cp + 1] << 8) | (short)(e.Buffer[cp] & 0x00ff));
                    cp += 2;
                    chartwaveinout.Series["SoundIn"].Points.Add(shortsArray[cp1]);
                }


                // 8 kHz, 16 bit's PCM frame's detection
                dtmfDetector.dtmfDetecting(shortsArray);
                if (dtmfDetector.getIndexDialButtons() > 0)
                {
                    char[] buttons = dtmfDetector.getDialButtonsArray();
                    for (int ii = 0; ii < dtmfDetector.getIndexDialButtons(); ++ii)
                        richTextBoxinout.Text += buttons[ii].ToString();
                    dtmfDetector.zerosIndexDialButtons();

                }


            }//else
        }//audio avaliab

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            waveIn.StopRecording();
        }









    }//class
}//namespace 
